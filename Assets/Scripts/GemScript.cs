using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GemScript : MonoBehaviour
{
    public int value;
    private GameManager GameManager;
    void Start()
    {
        //Accessing gamemanager
        GameObject gameManagerGO = GameObject.FindWithTag(GameConst.GameManagerTag);
        GameManager = gameManagerGO.GetComponent<GameManager>();
    }

    void OnTriggerEnter2D(Collider2D collider)
    {
        //If player touches the gem
        if (collider.tag==GameConst.PlayerTag)
        {
            GameManager.ShowText("+" + value, 25, Color.yellow, transform.position, Vector3.up * 25, 1.5f);
            //Adding money and updating score text
            GameManager.SetScore(value);
            //Destroying the gem
            Destroy(gameObject);
        }
    }
}
