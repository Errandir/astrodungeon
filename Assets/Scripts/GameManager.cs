using System.Collections;
using System.Collections.Generic;
using System.Runtime.CompilerServices;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class GameManager : MonoBehaviour
{
    public FloatingTextManager FloatingTextManager;
    public float respawnDelay;
    [SerializeField]
    private int PlayerHealth;
    private int PlayerMaxHealth = 3;
    private PlayerController gamePlayer;
    private int money=0;
    public Text scoreText;
    //public Text deadText;
    public AudioSource AudioSource;
    public Image[] heartSprites;
    //A list of collected keys
    public List<string> keys;
    //Sprites of available keys
    public Image[] keySprites;
    //A sprite of a key collected by player
    private Sprite pickedKeySprite;
    public Slider AudioSlider;
    public Image GunImage;
    public Image DoubleJumpImage;
    //public AudioSource GameOverAudio,PlayerDeathAudio,PickUpSoundAudio;
    public AudioClip GameOverAudio, PlayerDeathAudio, PickUpSoundAudio;
    private AudioSource audio;
    private bool playerCanShoot;
    private bool playerCanDoubleJump;
    public GameObject DeadPanel;

    void Start()
    {
        audio = GetComponent<AudioSource>();
        GameObject playerGO = GameObject.FindWithTag(GameConst.PlayerTag);
        gamePlayer = playerGO.GetComponent<PlayerController>();
        SetScore(GlobalControl.Instance.money);
        InitHealth();
        InitStats();
        AudioSource.volume = GlobalControl.Instance.AudioVolume;
        AudioSlider.value= GlobalControl.Instance.AudioVolume;
        keys = GlobalControl.Instance.keys;
        foreach (var key in keys)
        {
            ShowKeySpriteInPanel(key,false);
        }
        InitCollectibles();
    }
    public void ShowText(string msg, int font, Color color, Vector3 pos, Vector3 motion, float duration)
    {
        FloatingTextManager.Show(msg, font, color, pos, motion, duration);
    }
    private void InitStats()
    {
        playerCanShoot = GlobalControl.Instance.hasGun;
        if (playerCanShoot)
        {
            gamePlayer.ShowGun();
        }
        playerCanDoubleJump = GlobalControl.Instance.doubleJump;
    }
    private void InitCollectibles()
    {
        if (playerCanDoubleJump)
        {
            gamePlayer.amountOfJumps = 2;
        }
        else
        {
            gamePlayer.amountOfJumps = 1;
        }
        ShowCollectiblesImage("DoubleJump", playerCanDoubleJump);
        ShowCollectiblesImage("Gun",playerCanShoot);
    }
    public void ChangePlayerCanDoubleJump(bool onOff)
    {
        playerCanDoubleJump = onOff;
    }
    public void ChangePlayerCanShoot(bool onOff)
    {
        playerCanShoot = onOff;
    }

    public bool CheckIfPlayerCanDoubleJump()
    {
        return playerCanDoubleJump;
    }
    public bool CheckIfPlayerCanShoot()
    {
        return playerCanShoot;
    }
    //The key will send its sprite to gamemanager
    public void GetSpriteFromKey(Sprite sprite)
    {
        pickedKeySprite = sprite;
    }

    public void ShowCollectiblesImage(string collectible, bool onOff)
    {
        switch (collectible)
        {
            case "Gun":
                if (onOff)
                {
                    GunImage.color=Color.white;
                }
                else
                {
                    GunImage.color = Color.black;
                }
                break;
            case "DoubleJump":
                if (onOff)
                {
                    DoubleJumpImage.color=Color.white;
                }
                else
                {
                    DoubleJumpImage.color = Color.black;
                }
                break;
        }
    }
    public void Respawn()
    {
        StartCoroutine(RespawnCoroutine());
    }

    public void FinalDeath()
    {
        StartCoroutine(FinalDeathCoroutine());
    }
    private IEnumerator FinalDeathCoroutine()
    {
        PlaySound(GameOverAudio);
        gamePlayer.gameObject.SetActive(false);
        //ShowDeadText(true,"Game over!");
        ShowDeadPanel(true, "Game over!");
        yield return new WaitForSeconds(respawnDelay);
        SaveData();
        SceneManager.LoadScene("DeathScene");
    }

    private IEnumerator RespawnCoroutine()
    {
        PlaySound(PlayerDeathAudio);
        gamePlayer.gameObject.SetActive(false);
        //ShowDeadText(true);
        //DeadPanel.SetActive(true);
        //Text deadText = DeadPanel.GetComponentInChildren<Text>();
        //deadText.text= "You are dead!\nYou have " + PlayerHealth + " lives left.";
        ShowDeadPanel(true, "You are dead!\nYou have " + PlayerHealth + " lives left.");
        yield return new WaitForSeconds(respawnDelay);
        gamePlayer.transform.position = gamePlayer.respawnPoint;
        gamePlayer.gameObject.SetActive(true);
        //ShowDeadText(false);
        //DeadPanel.SetActive(false);
        ShowDeadPanel(false,"");
        InitStats();
    }

    private void ShowDeadPanel(bool onOff, string message)
    {
        DeadPanel.SetActive(onOff);
        Text deadText = DeadPanel.GetComponentInChildren<Text>();
        deadText.text = message;
    }
    public void SaveData()
    {
        GlobalControl.Instance.money = money;
        GlobalControl.Instance.keys = keys;
        GlobalControl.Instance.health = PlayerHealth;
        GlobalControl.Instance.AudioVolume = AudioSource.volume;
        GlobalControl.Instance.hasGun = playerCanShoot;
        GlobalControl.Instance.doubleJump = playerCanDoubleJump;
        //GlobalControl.Instance.savedPlayerData = localPlayerData;
    }

    private void InitHealth()
    {
        PlayerHealth = GlobalControl.Instance.health;
        ShowHeartSpriteInPanel(PlayerHealth);
    }
    public void ChangeHealth(int value)
    {
        PlayerHealth += value;
        if (PlayerHealth > PlayerMaxHealth)
        {
            PlayerHealth = PlayerMaxHealth;
        }
        ShowHeartSpriteInPanel(PlayerHealth);
    }

    public int GetHealth()
    {
        return PlayerHealth;
    }
    public void PickUpKey(string keyType)
    {
        PlaySound(PickUpSoundAudio);
        //Checking if the key is already picked up
        if (!IsKeyInTheInventory(keyType))
        {
            //Add it to the list
            keys.Add(keyType);
            //Show its sprite in the top panel
            ShowKeySpriteInPanel(keyType,false);
            //Debug.Log(keyType+" key picked up.");
        }
    }

    public void LooseKey(string keyType)
    {
        if (IsKeyInTheInventory(keyType))
        {
            keys.Remove(keyType);
            //Remove key sprite from panel
            ShowKeySpriteInPanel(keyType,true);
        }
    }
    //Checking if the key is already picked up
    public bool IsKeyInTheInventory(string key)
    {
        //Trying to get index of the key in a list
        int id = keys.IndexOf(key);
        //If the key is not found in the list
        if (id == -1)
            return false;
        return true;
    }
    //Showing the sprite in panel
    private void ShowKeySpriteInPanel(string keyType,bool delete)
    {
        if (!delete)
        {
            //Showing sprite
            //Depending on the color of the key
            if (keyType == "Green")
            {
                keySprites[0].color = Color.white;
            }
            if (keyType == "Red")
            {
                keySprites[1].color = Color.white;
            }
            if (keyType == "Blue")
            {
                keySprites[2].color = Color.white;
            }
        }
        else
        {
            //Hiding sprite
            //Depending on the color of the key
            if (keyType == "Green")
            {
                keySprites[0].color = Color.black;
            }
            if (keyType == "Red")
            {
                keySprites[1].color = Color.black;
            }
            if (keyType == "Blue")
            {
                keySprites[2].color = Color.black;
            }
        }
        
    }

    //Showing heart in panel
    private void ShowHeartSpriteInPanel(int id)
    {
        foreach (var heartSprite in heartSprites)
        {
            heartSprite.color = Color.black;
        }

        for (int i = 1; i <= id; i++)
        {
            heartSprites[i-1].color = Color.white;
        }
    }
    public int GetScore()
    {
        return money;
    }
    //Changing money and updating the score text
    public void SetScore(int income)
    {
        money += income;
        scoreText.text = "Score: " + money;
        if (income!=0)
        {
            PlaySound(PickUpSoundAudio);
        }
    }

    public void PlaySound(AudioClip audioClip)
    {
        audio.clip = audioClip;
        audio.Play();
    }
    /*private void ShowDeadText(bool onOff, string message)
    {
        deadText.text = message;
        deadText.enabled = onOff;
    }
    private void ShowDeadText(bool onOff)
    {
        deadText.text = "You are dead!\nYou have " + PlayerHealth + " lives left.";
        deadText.enabled= onOff;
    }*/
}
