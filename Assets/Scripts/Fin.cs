using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class Fin : MonoBehaviour
{
    public Canvas MainCanvas, FinCanvas;
    void OnTriggerEnter2D(Collider2D collider)
    {
        if (collider.tag==GameConst.PlayerTag)
        {
            MainCanvas.gameObject.SetActive(false);
            FinCanvas.gameObject.SetActive(true);
            StartCoroutine(Pause());
        }

    }

    IEnumerator Pause()
    {
        yield return new WaitForSeconds(3);
        SceneManager.LoadScene("DeathScene");
    }
    }
