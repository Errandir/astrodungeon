using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ApplicationQuitScript : MonoBehaviour
{
    public void Exit()
    {
        Application.Quit();
    }
}
