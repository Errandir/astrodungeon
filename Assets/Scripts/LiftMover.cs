using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LiftMover : MonoBehaviour
{
    public float offsetSmoothing;
    public float moveFromX, moveToX, moveFromY,moveToY;
    public bool backAndForth=false;
    void Start()
    {
        //ResetPosition();
    }

    void Update()
    {
        if (!backAndForth)
        {
            if (moveToX == moveFromX)
            {
                //Moving vertically => checking Y-axis
                if (transform.position.y == moveToY)
                {
                    ResetPosition();
                }
            }
            else if (moveFromY == moveToY)
            {
                //Moving horizontally => checking X-axis
                if (transform.position.x == moveToX)
                {
                    ResetPosition();
                }
            }
            else
            {
                //Moving diagonally => checking both axis
                if (transform.position.x == moveToX && transform.position.y == moveToY)
                {
                    ResetPosition();
                }
            }
        }
        else
        {
            if (moveToX == moveFromX)
            {
                //Moving vertically => checking Y-axis
                if (transform.position.y == moveToY)
                {
                    float temp = moveToY;
                    moveToY = moveFromY;
                    moveFromY = temp;
                }
            }
            else if (moveFromY == moveToY)
            {
                //Moving horizontally => checking X-axis
                if (transform.position.x == moveToX)
                {
                    float temp = moveToX;
                    moveToX = moveFromX;
                    moveFromX = temp;
                }
            }
            else
            {
                //Moving diagonally => checking both axis
                if (transform.position.x == moveToX && transform.position.y == moveToY)
                {
                    float tempY = moveToY;
                    float tempX = moveToX;
                    moveToY = moveFromY;
                    moveToX = moveFromX;
                    moveFromY = tempY;
                    moveFromX = tempX;
                }
            }
        }
        Move();
    }
    void Move()
    {
        transform.position = Vector3.MoveTowards(transform.position, new Vector3(moveToX, moveToY, 0), offsetSmoothing * Time.deltaTime);
    }

    void ResetPosition()
    {
        gameObject.transform.position = new Vector3(moveFromX, moveFromY);
    }
}
