using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DoorOpener : MonoBehaviour
{
    private AudioSource doorSource;
    void Start()
    {
        doorSource = GetComponent<AudioSource>();
    }
    void OnTriggerEnter2D(Collider2D collider)
    {
        //Checking if it is the player who touches the door
        if (collider.tag==GameConst.PlayerTag)
        {
            //Finding trigger collider
            EdgeCollider2D triggerCollider = GetComponent<EdgeCollider2D>();
            //And disabling in on very first touch
            triggerCollider.enabled = false;     
            //Playing open door sound
            doorSource.Play();
            StartCoroutine(OpenDoor());
        }
    }
    //Open door by rotating it around Y-axis
    IEnumerator OpenDoor()
    {
        for (int i = 0; i < 9; i++)
        {
            transform.Rotate(0f, 10f, 0f, Space.Self);
            yield return new WaitForSeconds(0.1f);
        }
        //Once the door is turned 90 degrees disabling it
        gameObject.SetActive(false);
    }
}
