using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CrateScript : MonoBehaviour,IDamagable
{
    public int CrateHealth = 3;
    private Animator animator;
    void Start()
    {
        animator = GetComponent<Animator>();
        animator.SetInteger("CrateHealth", CrateHealth);
    }
    public void Damage()
    {
        CrateHealth--;
        animator.SetInteger("CrateHealth",CrateHealth);
    }
}
