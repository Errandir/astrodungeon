using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Enemy : MonoBehaviour,IDamagable
{
    [Header("Private serialized bools")]
    [SerializeField] private bool isFacingRight = true;
    [SerializeField] private bool isTouchingWall;
    [SerializeField] private bool isPlayerDetected = false;
    [Header("Enemy Stats")]
    public bool startFacingLeft;
    public float enemySpeed;
    public Transform wallCheckPoint;
    public float wallCheckDistance;
    public LayerMask groundLayer;
    //Specific boxcollider to detect death
    [Header("BoxCollider2D to detect death")]
    public BoxCollider2D boxCollider;
    private CircleCollider2D playerDetector;
    private GameObject player;
    public GameObject projectile;
    public GameObject rewardGem;
    public Transform gunPoint;
    public float fireRate;
    private float nextShot = 0f;
    public bool enemyCanShoot;
    public AudioSource enemyExplosionAudio;
    public Animator animation;
    private bool enemyAlive = true;
    void Start()
    {
        animation = GetComponent<Animator>();
        if (startFacingLeft)
        {
            Flip();
        }
        //Finding player to keep track of his movement
        player = GameObject.FindWithTag(GameConst.PlayerTag);
        //Accessing PlayerDetector - a collider to detect player
        playerDetector = GetComponentInChildren<CircleCollider2D>();
    }

    private void Shoot()
    {
        if (enemyCanShoot && Time.time > nextShot)
        {
            nextShot = Time.time + fireRate; 
            GameObject bullet = Instantiate(projectile, gunPoint.position, Quaternion.identity) as GameObject;
            BulletScript enemyBullet = bullet.GetComponent<BulletScript>();
            //Setting direction of a bullet to fly
            enemyBullet.SetDirection(isFacingRight);
        }
    }
    private void FixedUpdate()
    {
        //Player is detected - attack mode
        if (isPlayerDetected)
        {
            //Moving enemy towards player
            transform.position = Vector2.MoveTowards(transform.position, new Vector2(player.transform.position.x, transform.position.y), enemySpeed * Time.deltaTime);
            //if the player is to the left and the enemy is facing right
            if (player.transform.position.x < transform.position.x && isFacingRight)
            {
                Flip();
            }
            //else if tre player is to the right and the enemy is facing left
            else if (player.transform.position.x > transform.position.x && !isFacingRight)
            {
                Flip();
            }
            if (enemyCanShoot)
            {
                Shoot();
            }
        }
        else //player is not detected - roaming mode
        {
            if (isFacingRight)
            {
                transform.position += new Vector3(enemySpeed * Time.deltaTime, 0f, 0f);
            }
            else
            {
                transform.position += new Vector3(-enemySpeed * Time.deltaTime, 0f, 0f);
            }
        }
        //Checking wall touching
        isTouchingWall = Physics2D.Raycast(wallCheckPoint.position, transform.right, wallCheckDistance, groundLayer);
        //If so - rotating
        if (isTouchingWall)
        {
            Flip();
        }
    }

    private void Flip()
    {
        isFacingRight = !isFacingRight;
        transform.Rotate(0f, 180f, 0f);
    }

    private void OnTriggerEnter2D(Collider2D collider)
    {
        //If enemy touches something with InstantDeath tag
        if (collider.tag==GameConst.DeathTag&&collider.IsTouching(boxCollider))
        {
            //Destroy enemy
            Destroy(gameObject);
        }
        //If PlayerDetector child gameobject touching player
        if (collider.IsTouching(playerDetector)&&collider.tag==GameConst.PlayerTag)
        {
            //Switching to chasing mode
            isPlayerDetected = true;
            enemySpeed = 2;
        }
    }

    public void Damage()
    {
        if (enemyAlive==true)
        {
            //Disabling colliders so player cannot move or kill himself by dying enemy
            foreach (var go in gameObject.GetComponents<Collider2D>())
            {
                go.enabled = false;
            }
            //Disabling gravity so dying enemy doesn't fall through the floor without colliders
            gameObject.GetComponent<Rigidbody2D>().gravityScale = 0f;
            //Stop enemy from shooting
            enemyCanShoot = false;
            //Stopping the enemy from moving
            enemySpeed = 0f;
            //Starting explosion animation
            animation.SetBool("EnemyHit", true);
            enemyExplosionAudio.Play();
            //Should turn off child gameobject with engine exhaust
            Transform engineFire = gameObject.transform.Find("EngineFire");
            engineFire.gameObject.SetActive(false);
            //Spawning reward gem
            GameObject coinGameObject = Instantiate(rewardGem, transform.position, Quaternion.identity);
            enemyAlive = false;
        }

    }
    private void OnTriggerExit2D(Collider2D collider)
    {
        //When PlayerDetector don't collide with player no more
        if (collider.tag == GameConst.PlayerTag)
        {
            //Switching to roaming mode
            enemySpeed = 1;
            isPlayerDetected = false;
        }
    }
    private void OnDrawGizmos()
    {
        Gizmos.DrawLine(wallCheckPoint.position, new Vector3(wallCheckPoint.position.x + wallCheckDistance, wallCheckPoint.position.y, wallCheckPoint.position.z));
    }
}
