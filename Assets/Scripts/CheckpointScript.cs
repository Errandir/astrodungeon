using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class CheckpointScript : MonoBehaviour
{
    public Sprite portalActivated;
    private SpriteRenderer checkpointSpriteRenderer;
    public bool checkpointReached;
    public string sceneName;
    private GameManager GameManager;
    void Start()
    {
        checkpointSpriteRenderer = GetComponent<SpriteRenderer>();
        GameObject gameManagerGO = GameObject.FindWithTag(GameConst.GameManagerTag);
        GameManager = gameManagerGO.GetComponent<GameManager>();
    }

    void OnTriggerEnter2D(Collider2D other)
    {
        if (other.tag == GameConst.PlayerTag)
        {
            checkpointSpriteRenderer.sprite = portalActivated;
            checkpointReached = true;
            GameManager.SaveData();
            SceneManager.LoadScene(sceneName);
        }
    }
}
