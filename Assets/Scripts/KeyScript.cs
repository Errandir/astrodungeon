using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class KeyScript : MonoBehaviour
{
    public string keyType;
    public float anglePerFrame;
    private GameManager GameManager;
    public Sprite keySprite;
    void Start()
    {
        //Gamemanager stores all keys collected by player
        GameObject gameManagerGO = GameObject.FindWithTag(GameConst.GameManagerTag);
        GameManager = gameManagerGO.GetComponent<GameManager>();
    }

    void Update()
    {
        //Simply rotating the key
        transform.Rotate(0f, anglePerFrame, 0f, Space.Self);
    }

    void OnTriggerEnter2D(Collider2D collider)
    {
        //If player touches the key
        if (collider.tag == GameConst.PlayerTag)
        {
            //Debug.Log("Touched key");
            //Sending sprite to gamemanager
            GameManager.GetSpriteFromKey(keySprite);
            //Picking up the key
            GameManager.PickUpKey(keyType);
            //And destroying it after
            Destroy(gameObject);
        }
    }
}
