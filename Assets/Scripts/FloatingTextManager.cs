using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class FloatingTextManager : MonoBehaviour
{
    public GameObject TextContainer;
    public GameObject TextPrefab;
    private List<FloatingText> floatingTexts=new List<FloatingText>();

    void Start()
    {
        DontDestroyOnLoad(gameObject);
    }
    private void Update()
    {
        foreach (FloatingText txt in floatingTexts)
        {
            txt.UpdateFloatingText();
        }
    }
    public void Show(string message,int fontSize,Color color, Vector3 position, Vector3 motion, float duration)
    {
        FloatingText floatingText = GetFloatingText();
        floatingText.Text.text = message;
        floatingText.Text.fontSize = fontSize;
        floatingText.Text.color = color;
        floatingText.GO.transform.position = Camera.main.WorldToScreenPoint(position);
        floatingText.Motion = motion;
        floatingText.Duration = duration;
        floatingText.Show();
    }
    private FloatingText GetFloatingText()
    {
        FloatingText txt = floatingTexts.Find(t => !t.Active);
        if (txt == null)
        {
            txt=new FloatingText();
            txt.GO = Instantiate(TextPrefab);
            txt.GO.transform.SetParent(TextContainer.transform);
            txt.Text = txt.GO.GetComponent<Text>();
            floatingTexts.Add(txt);
        }
        return txt;
    }
}
