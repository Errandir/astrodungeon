using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class GlobalControl : MonoBehaviour
{
    public static GlobalControl Instance;
    public int money;
    public int health;
    public float AudioVolume;
    public bool doubleJump;
    public bool hasGun;
    //public PlayerStatistics savedPlayerData = new PlayerStatistics();
    public List<string> keys;
    void Awake()
    {
        if (Instance == null)
        {
            DontDestroyOnLoad(gameObject);
            Instance = this;
        }
        else if (Instance != this)
        {
            Destroy(gameObject);
        }
    }

}
