using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Video;

public class TurnOnTV : MonoBehaviour
{
    private VideoPlayer videoPlayer;
    private Collider2D triggerCollider2D;

    void Start()
    {
        videoPlayer = GetComponentInChildren<VideoPlayer>();
        triggerCollider2D = GetComponentInChildren<Collider2D>();
    }
    private void OnTriggerEnter2D(Collider2D collision)
    {
        if(collision.tag==GameConst.PlayerTag)
            videoPlayer.Play();
    }
}
