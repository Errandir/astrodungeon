using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerStatistics
{
    public int Money;
    public int Health;
    public float AudioVolume;
    public List<string> Keys=new List<string>();
}
