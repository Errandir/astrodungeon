using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Rotator : MonoBehaviour
{
    public float anglePerFrame;
    // Update is called once per frame
    void Update()
    {
        transform.Rotate(0f, anglePerFrame, 0f, Space.Self);
    }
}
