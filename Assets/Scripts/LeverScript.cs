using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LeverScript : MonoBehaviour
{
    public Sprite leverUp, leverDown;
    public GameObject platform;
    public string keyNeeded;
    public float turningSpeed;
    public float hideAfter;
    private RotateAroundPivot script;
    private SpriteRenderer leverSpriteRenderer;
    private GameManager GameManager;
    private GameObject popupPanel;
    void Start()
    {
        //SpriteRenderer to change sprite of the lever
        leverSpriteRenderer = GetComponent<SpriteRenderer>();
        //GameManager stores keys needed to pull the lever
        GameObject gameManagerGO = GameObject.FindWithTag(GameConst.GameManagerTag);
        GameManager = gameManagerGO.GetComponent<GameManager>();
        //Script for changing turning speed of a platform
        script = platform.GetComponent<RotateAroundPivot>();
        //First child gameobject is a panel with text
        popupPanel = gameObject.transform.GetChild(0).gameObject;
    }

    void OnTriggerEnter2D(Collider2D other)
    {
        //Checking if player has the needed key
        bool isKeyInTheInventory = GameManager.IsKeyInTheInventory(keyNeeded);
        //If player touches the lever
        if (other.tag == GameConst.PlayerTag)
            //And if he has the required key
            if (isKeyInTheInventory)
            {
                //Changing lever sprite to down and starting platform rotation
                leverSpriteRenderer.sprite = leverDown;
                script.turningSpeed = turningSpeed;
                GameManager.LooseKey(keyNeeded);
            }
            else
            {
                //Showing popup panel
                popupPanel.SetActive(true);
                //Accessing "plate" child component of the panel
                GameObject text= popupPanel.transform.GetChild(0).gameObject;
                //Accessing plate's mesh renderer
                MeshRenderer meshTest=text.GetComponent<MeshRenderer>();
                //And changing its sorting layer because we cannot do it from the inspector
                meshTest.sortingLayerName = "Midground";
                //Accessing text mesh component
                TextMesh textMesh = text.GetComponent<TextMesh>();
                //Setting text according to the needed key
                textMesh.text = keyNeeded + " key is needed \nto pull this lever!";
                //Hiding panel after amount of time
                StartCoroutine(HidePanel());
            }
    }

    IEnumerator HidePanel()
    {
        //Waiting for seconds and disabling the panel after
        yield return new WaitForSeconds(hideAfter);
        popupPanel.SetActive(false);
    }
}
