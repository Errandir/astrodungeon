using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public static class GameConst
{
    public const string EnemyTag = "Enemy";
    public const string DoorTag = "Doors";
    public const string DeathTag = "InstantDeath";
    public const string LadderTag = "Ladder";
    public const string KeyTag = "Key";
    public const string LeverTag = "Lever";
    public const string HealthTag = "Health";
    public const string DoubleJumpTag = "DoubleJump";
    public const string GunTag = "Gun";
    public const string PlayerTag = "Player";
    public const string TurnOffTag = "TurnOff";
    public const string DestructibleTag = "Destructible";
    public const string GameManagerTag = "GameManager";

    public const string ScoreFilePath = "Assets/score.dat";
}
