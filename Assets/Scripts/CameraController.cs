using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraController : MonoBehaviour
{
    public GameObject player;

    public float offset;

    private Vector3 playerPosition;
    private PlayerController playerController;
    public float offsetSmoothing;

    void Start()
    {
        playerController = player.GetComponent<PlayerController>();
    }
    void Update()
    {
        playerPosition = new Vector3(player.transform.position.x, transform.position.y, transform.position.z);
        //If player moving right the offset is plus
        if (/*player.transform.localScale.x > 0f*/playerController.IsFacingRight())
        {
            playerPosition = new Vector3(playerPosition.x + offset, playerPosition.y, playerPosition.z);
        }
        //If player moving left the offset is minus
        else
        {
            playerPosition = new Vector3(playerPosition.x - offset, playerPosition.y, playerPosition.z);
        }

        transform.position = Vector3.Lerp(transform.position, playerPosition, offsetSmoothing * Time.deltaTime);

    }
}
