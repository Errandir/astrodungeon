using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RotateAroundPivot : MonoBehaviour
{
    public Vector3 Pivot;
    public float turningSpeed;
    //Boolean to stop rotation
    public bool stopRotation = false;
    //it could be a Vector3 but its more user friendly
    public bool RotateX = false;
    public bool RotateY = false;
    public bool RotateZ = false;

    void FixedUpdate()
    {
        //Rotating until ordered to stop
        if (stopRotation==false)
        {
            transform.position += (transform.rotation * Pivot);
            if (RotateX)
                transform.rotation *= Quaternion.AngleAxis(turningSpeed * Time.deltaTime, Vector3.right);
            if (RotateY)
                transform.rotation *= Quaternion.AngleAxis(turningSpeed * Time.deltaTime, Vector3.up);
            if (RotateZ)
                transform.rotation *= Quaternion.AngleAxis(turningSpeed * Time.deltaTime, Vector3.forward);
            transform.position -= (transform.rotation * Pivot);
        }
    }

    void OnTriggerEnter2D(Collider2D collider)
    {
        //Debug.Log("Trigger");
        //Checking collision with TurnOff point
        if (collider.tag == GameConst.TurnOffTag)
        {
            //Debug.Log("Triggered");
            //Sending stop rotation signal
            stopRotation = true;
            //gameObject.SetActive(false);
        }
    }

}
