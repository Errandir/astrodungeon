using System.Collections;
using System.Collections.Generic;
using System.Net.Http.Headers;
using UnityEngine;
using UnityEngine.SceneManagement;

public class PlayerController : MonoBehaviour,IDamagable
{
    public float speed = 5f;

    [SerializeField] private float movementX = 0f;
    [SerializeField] private float movementY = 0f;
    [SerializeField] private int amountOfJumpsLeft;

    public int amountOfJumps = 1;
    public float jumpSpeed = 5f;
    private Rigidbody2D rigidbody2D;

    [SerializeField] private bool isTouchingGround;
    [SerializeField] private bool canJump;
    [SerializeField] private bool canClimb = false;
    [SerializeField] private bool playerCanShoot;
    public Transform groundCheckPoint;
    public float groundCheckRadius;
    public LayerMask groundLayer;
    public Transform wallCheckPoint;
    public float wallCheckDistance;
    private Animator playerAnimation;

    private GameManager GameManager;

    public Vector3 respawnPoint;

    private bool isFacingRight = true;

    public AudioSource SpawnAudioSource, JumpAudioSource, ShotAudioSource;
    public GameObject gun;
    public GameObject projectile;
    public Transform gunPoint;
    public float fireRate;
    private float nextShot=0f;
    void Start()
    {
        rigidbody2D = GetComponent<Rigidbody2D>();
        playerAnimation = GetComponent<Animator>();
        GameObject gameManagerGO = GameObject.FindWithTag(GameConst.GameManagerTag);
        GameManager = gameManagerGO.GetComponent<GameManager>();
        respawnPoint = transform.position;
        amountOfJumpsLeft = amountOfJumps;
        SpawnAudioSource.Play();
        playerCanShoot = GameManager.CheckIfPlayerCanShoot();
        if (playerCanShoot)
        {
            ShowGun();
        }

    }
    private void FixedUpdate()
    {
    }
    void Update()
    {
        movementX = Input.GetAxis("Horizontal");
        movementY = Input.GetAxis("Vertical");
        rigidbody2D.velocity = new Vector2(movementX * speed, rigidbody2D.velocity.y);
        //Checking ground touching
        isTouchingGround = Physics2D.OverlapCircle(groundCheckPoint.position, groundCheckRadius, groundLayer);
        if (Input.GetButtonDown("Jump") && canJump)
        {
            Jump();
        }
        //Moving up
        if (movementY != 0f && canClimb)
        {
            rigidbody2D.velocity = new Vector2(rigidbody2D.velocity.x, movementY * speed);
        }
        //Rotating player from right to left
        if (movementX < 0f && isFacingRight)
            Flip();
        else if (movementX > 0f && !isFacingRight)
            Flip();
        UpdateAnimations();
        //Checking if can jump
        /*if (isTouchingGround && rigidbody2D.velocity.y <= 0)
        {
            //Resetting amount of jumps when touching ground
            amountOfJumpsLeft = amountOfJumps;
        }
        if (amountOfJumpsLeft <= 0)
        {
            canJump = false;
        }
        else
        {
            canJump = true;
        }*/
        CheckIfCanJump();
        if (Input.GetKeyDown(KeyCode.LeftShift)&&playerCanShoot&&Time.time>nextShot)
        {
            nextShot = Time.time + fireRate;
            GameObject bullet = Instantiate(projectile, gunPoint.position, Quaternion.identity) as GameObject;
            BulletScript bulletScript = bullet.GetComponent<BulletScript>();
            bulletScript.SetDirection(isFacingRight);
            ShotAudioSource.Play();
        }
    }

    public void ShowGun()
    {
        gun.SetActive(true);
        playerCanShoot = true;
    }
    public bool IsFacingRight()
    {
        return isFacingRight;
    }
    private void Jump()
    {
        rigidbody2D.velocity += new Vector2(rigidbody2D.velocity.x, jumpSpeed);
        amountOfJumpsLeft--;
        JumpAudioSource.Play();
    }

    private void CheckIfCanJump()
    {
        if (isTouchingGround && rigidbody2D.velocity.y<=0)
        {
            //Resetting amount of jumps when touching ground
            amountOfJumpsLeft = amountOfJumps;
        }
        if (amountOfJumpsLeft<=0)
        {
            canJump = false;
        }
        else
        {
            canJump = true;
        }
    }
    private void Flip()
    {
        isFacingRight = !isFacingRight;
        transform.Rotate(0f,180f,0f);
    }
    
    private void UpdateAnimations()
    {
        playerAnimation.SetFloat("Speed", Mathf.Abs(movementX));
        playerAnimation.SetBool("OnGround", isTouchingGround);
        playerAnimation.SetFloat("SpeedY", Mathf.Abs(movementY));
        playerAnimation.SetBool("CanClimb", canClimb);
    }

    public void Damage()
    {
        GameManager.ChangeHealth(-1);
        if (GameManager.GetHealth() > 0)
        {
            GameManager.Respawn();
        }
        else
        {
            GameManager.FinalDeath();
        }
    }
    private void OnTriggerEnter2D(Collider2D collider)
    {
        if (collider.tag == GameConst.DeathTag||collider.tag==GameConst.EnemyTag)
        {
            Damage();
        }
        //Picking up Health prefab
        if (collider.tag==GameConst.HealthTag)
        {
            GameManager.PlaySound(GameManager.PickUpSoundAudio);
            GameManager.ChangeHealth(1);
            Destroy(collider.gameObject);
        }
        //Picking up DoubleJump
        if (collider.tag == GameConst.DoubleJumpTag)
        {
            GameManager.PlaySound(GameManager.PickUpSoundAudio);
            GameManager.ShowCollectiblesImage("DoubleJump",true);
            GameManager.ChangePlayerCanDoubleJump(true);
            amountOfJumps = 2;
            GameManager.SaveData();
            Destroy(collider.gameObject);
        }
        //Picking up Gun
        if (collider.tag == GameConst.GunTag)
        {
            GameManager.PlaySound(GameManager.PickUpSoundAudio);
            GameManager.ShowCollectiblesImage("Gun", true);
            GameManager.ChangePlayerCanShoot(true);
            playerCanShoot = GameManager.CheckIfPlayerCanShoot();
            ShowGun();
            GameManager.SaveData();
            Destroy(collider.gameObject);
        }
    }

    private void OnTriggerExit2D(Collider2D collider)
    {
        if (collider.tag==GameConst.LadderTag)
        {
            canClimb = false;
        }
    }
    private void OnTriggerStay2D(Collider2D collider)
    {
        if (collider.tag==GameConst.LadderTag)
        {
            canClimb = true;
        }
    }

    private void OnDrawGizmos()
    {
        Gizmos.DrawWireSphere(groundCheckPoint.position,groundCheckRadius);
        Gizmos.DrawLine(wallCheckPoint.position,new Vector3(wallCheckPoint.position.x+wallCheckDistance,wallCheckPoint.position.y, wallCheckPoint.position.z));
    }
    }
