using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ShowMessage : MonoBehaviour
{
    public string textToShow;
    public float hideAfter;
    private GameObject popupPanel;

    void Start()
    {
        popupPanel = gameObject.transform.GetChild(0).gameObject;
        textToShow = textToShow.Replace("\\n", "\n");
    }

    void OnTriggerEnter2D(Collider2D other)
    {
        //If player touches the lever
        if (other.tag == GameConst.PlayerTag)
        {
            //Showing popup panel
            popupPanel.SetActive(true);
            //Accessing "plate" child component of the panel
            GameObject text = popupPanel.transform.GetChild(0).gameObject;
            //Accessing plate's mesh renderer
            MeshRenderer meshTest = text.GetComponent<MeshRenderer>();
            //And changing its sorting layer because we cannot do it from the inspector
            meshTest.sortingLayerName = "Midground";
            //Accessing text mesh component
            TextMesh textMesh = text.GetComponent<TextMesh>();
            //Setting text according to the needed key
            textMesh.text = textToShow;
            //Hiding panel after amount of time
            StartCoroutine(HidePanel());
        }
    }

    IEnumerator HidePanel()
    {
        //Waiting for seconds and disabling the panel after
        yield return new WaitForSeconds(hideAfter);
        popupPanel.SetActive(false);
        //And disabling message gameobject to prevent repeated triggering
        gameObject.SetActive(false);
    }
}
