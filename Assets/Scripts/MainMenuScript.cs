using System;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using UnityEditor;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class MainMenuScript : MonoBehaviour
{
    public float TimeDelayForMenu;
    public AudioSource AudioSource;
    public Text ScoreText;
    public GameObject NameEnterPanel;
    public InputField NameInputField;
    public Text PlayerScoreText, PlayerNameText;
    private Scene scene;
    void Start()
    {
        scene = SceneManager.GetActiveScene();
        if (scene.name=="DeathScene")
        {
            ReadString();
        }
        StartCoroutine(ExpandMenu());
        if (ScoreText != null)
            ScoreText.text = GlobalControl.Instance.money.ToString();
    }

    public void StartNewGame()
    {
        InitPlayerStats();
        //GlobalControl.Instance.savedPlayerData.AudioVolume = AudioSource.volume;
        SceneManager.LoadScene("Intro");
    }

    public void RestartGame()
    {
        InitPlayerStats();
        SceneManager.LoadScene("Scene_1");
    }
    private void InitPlayerStats()
    {
        GlobalControl.Instance.AudioVolume = AudioSource.volume;
        GlobalControl.Instance.money = 0;
        GlobalControl.Instance.health = 3;
    }
    public void Exit()
    {
        Application.Quit();
    }
    private void EnableDisableButtons(bool stateActivePassive)
    {
        for (int i = 0; i < gameObject.transform.childCount; i++)
        {
            GameObject child = gameObject.transform.GetChild(i).gameObject;
            child.SetActive(stateActivePassive);
        }
    }

    private void ChangeButtonsSize(int size)
    {
            GameObject child = gameObject.transform.GetChild(1).gameObject;
            RectTransform rt = child.GetComponent<RectTransform>();
            rt.sizeDelta = new Vector2(size * 4f, rt.sizeDelta.y);
            for (int j = 0; j < child.transform.childCount; j++)
            {
                GameObject childOfChild = child.transform.GetChild(j).gameObject;
                RectTransform rtOfChild = childOfChild.GetComponent<RectTransform>();
                rtOfChild.sizeDelta = new Vector2(size * 4f, rtOfChild.sizeDelta.y);
            }
    }

    private void ChangeLogoSize(int size)
    {
        GameObject child = gameObject.transform.GetChild(0).gameObject;
        RectTransform rt = child.GetComponent<RectTransform>();
        rt.sizeDelta = new Vector2(size * 4f, rt.sizeDelta.y);
        child = gameObject.transform.GetChild(2).gameObject;
        rt = child.GetComponent<RectTransform>();
        rt.sizeDelta = new Vector2(size * 4f, rt.sizeDelta.y);
    }
    IEnumerator ExpandMenu()
    {
        RectTransform rt = gameObject.GetComponent<RectTransform>();
        rt.sizeDelta = new Vector2(1f, rt.sizeDelta.y);
        for (int i = 1; i < 65; i++)
        {
            rt.sizeDelta = new Vector2(i*5, rt.sizeDelta.y);
            ChangeLogoSize(i);
            ChangeButtonsSize(i);
            yield return new WaitForSeconds(TimeDelayForMenu);
        }
        yield return new WaitForSeconds(0.5f);
        if (scene.name == "DeathScene")
        {
            gameObject.SetActive(false);
            NameEnterPanel.SetActive(true);
        }
    }
    public void WriteString()
    {
        StreamWriter writer = new StreamWriter(GameConst.ScoreFilePath, true);
        string name = NameInputField.text;
        if (name==""||name==" "||name==";")
        {
            name = "Unknown";
        }
        string score = ScoreText.text;
        writer.WriteLine(name+";"+score);
        writer.Close();
    }

    public void ReadString()
    {
        PlayerScoreText.text = "";
        PlayerNameText.text = "";
        StreamReader reader = new StreamReader(GameConst.ScoreFilePath);
        List<PlayerScore> list=new List<PlayerScore>();
        while (!reader.EndOfStream)
        {
            string line = reader.ReadLine();
            string[] words = line.Split(';');
            PlayerScore ps=new PlayerScore();
            ps.Name = words[0];
            try
            {
                ps.Score = Convert.ToInt32(words[1]);
            }
            catch
            {
                Debug.Log("Score.dat corrupted");
                ps.Score = -1;
            }
            list.Add(ps);
        }
        SortByScore(ref list);
        foreach (var lPlayerScore in list)
        {
            PlayerNameText.text += lPlayerScore.Name + "\n";
            PlayerScoreText.text += lPlayerScore.Score + "\n";
        }
        reader.Close();
    }

    private void SortByScore(ref List<PlayerScore> list)
    {
        for (int i = 0; i < list.Count - 1; i++)
        {
            for (int j = i + 1; j < list.Count; j++)
            {
                if (list[i].Score < list[j].Score)
                {
                    PlayerScore temp = list[i];
                    list[i] = list[j];
                    list[j] = temp;
                }
            }
        }
    }
    private class PlayerScore
    {
        public string Name { get; set; }
        public int Score { get; set; }
    }
}
