using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemyBullet : MonoBehaviour
{
    public float bulletSpeed;
    private float direction;

    public void SetDirection(bool x)
    {
        if (x)
        {
            direction = 1f;
        }
        else
        {
            direction = -1f;
        }

    }
    void FixedUpdate()
    {
        transform.position += new Vector3(bulletSpeed * direction * Time.fixedDeltaTime, 0, 0f);
    }

    void OnTriggerEnter2D(Collider2D collider)
    {
        IDamagable damagable= collider.GetComponent<IDamagable>();
        if (damagable != null)
        {
            damagable.Damage();
            Destroy(gameObject);
        }
        /*if (collider.tag == GameConst.PlayerTag)
        {
            player = collider.GetComponent<PlayerController>();
            if (player!=null)
            {
                player.Damage();
                Destroy(gameObject);
            }
        }*/

        if (collider.tag == GameConst.DeathTag)
        {
            //Debug.Log("Destroyed after leaving the scene");
            Destroy(gameObject);
        }
        if (collider.gameObject.layer == 3)
        {
            //Debug.Log("Hit ground layer");
            Destroy(gameObject);
            //Time.timeScale = 0;
        }
        //Destroy(gameObject);
    }
}
