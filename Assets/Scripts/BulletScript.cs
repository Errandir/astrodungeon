using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BulletScript : MonoBehaviour
{
    public float BulletSpeed;
    private float direction;
    private Rigidbody2D rb2d;

    private void Start()
    {
        rb2d = GetComponent<Rigidbody2D>();
    }
    public void SetDirection(bool x)
    {
        if (x)
        {
            direction = 1f;
        }
        else
        {
            direction = -1f;
        }
    }
    void FixedUpdate()
    {
        transform.position += new Vector3(BulletSpeed*direction * Time.fixedDeltaTime, 0, 0f);
        //Vector2 velocity=new Vector2(1f,0f);
        //GetComponent<Rigidbody2D>().MovePosition(rb2d.position+ new Vector2(1f, 0f) * BulletSpeed * direction * Time.deltaTime);
    }

    void OnTriggerEnter2D(Collider2D collider)
    { 
        IDamagable damagable = collider.GetComponent<IDamagable>();
        if (damagable != null)
        {
            damagable.Damage();
            Destroy(gameObject);
        }
        //If the bullet touches screen boundaries
        if (collider.tag==GameConst.DeathTag)
        {
            //Destroy it
            Destroy(gameObject);
        }
        //If the bullet touches ground layer
        if (collider.gameObject.layer == 3)
        {
            //Destroy it
            Destroy(gameObject);
        }
    }
}
