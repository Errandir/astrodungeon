using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Turret : MonoBehaviour
{
    public GameObject projectile;
    public Transform gunPoint;
    public LayerMask hittableLayer;
    public float detectDistance;
    public float fireRate;
    private float nextShot = 0f;
    public bool isFacingRight;
    private Vector2 directionOfShooting;
    void Start()
    {
        if (isFacingRight)
        {
            directionOfShooting=Vector2.right;
        }
        else
        {
            directionOfShooting=Vector2.left;
        }
    }

    void Update()
    {
        /*RaycastHit2D hit= Physics2D.Raycast(gunPoint.position, Vector2.right, detectDistance, hittableLayer);
        if (hit != null)
            if (hit.collider.tag=="Player")
            {
                Debug.Log(hit.collider.tag);
            }*/
        if (Physics2D.Raycast(gunPoint.position, directionOfShooting, detectDistance, hittableLayer))
            Shoot();
            //Debug.Log("Touched hittable layer object");
            
    }
    private void Shoot()
    {
        if (Time.time > nextShot)
        {
            nextShot = Time.time + fireRate;
            GameObject bullet = Instantiate(projectile, gunPoint.position, Quaternion.identity) as GameObject;
            BulletScript enemyBullet = bullet.GetComponent<BulletScript>();
            enemyBullet.SetDirection(isFacingRight);
            //ShotAudioSource.Play();
        }
    }
    private void OnDrawGizmos()
    {
        Gizmos.DrawLine(gunPoint.position, new Vector3(gunPoint.position.x + detectDistance, gunPoint.position.y, gunPoint.position.z));
    }
}
